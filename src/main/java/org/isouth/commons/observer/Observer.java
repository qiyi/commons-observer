package org.isouth.commons.observer;

public interface Observer<E> {
    void update(String subject, E event);
}
