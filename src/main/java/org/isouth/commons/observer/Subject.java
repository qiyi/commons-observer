package org.isouth.commons.observer;

import java.util.LinkedList;
import java.util.List;

public class Subject<E> {
    private String name;

    private List<Observer<E>> observers = new LinkedList<>();

    public Subject(String name) {
        this.name = name;
    }

    public void addObserver(Observer<E> o) {
        observers.add(o);
    }

    public void deleteObserver(Observer<E> o) {
        observers.remove(o);
    }

    public void deleteObservers() {
        observers.clear();
    }

    public void notifyObservers(E event) {
        List<Observer<E>> clone = new LinkedList<>(observers);
        for (Observer<E> o : clone) {
            o.update(this.name, event);
        }
    }
}
