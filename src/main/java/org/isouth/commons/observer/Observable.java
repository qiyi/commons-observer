package org.isouth.commons.observer;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class Observable<E> {

    private ConcurrentMap<String, Subject<E>> subjects = new ConcurrentHashMap<String, Subject<E>>();

    public void addObserver(Observer<E> o, String sub) {
        addObserver(o, Arrays.asList(sub));
    }

    public void addObserver(Observer<E> o, List<String> subs) {
        for (String sub : subs) {
            Subject<E> subject = subjects.get(sub);
            if (subject == null) {
                subjects.putIfAbsent(sub, new Subject<E>(sub));
                subject = subjects.get(sub);
            }
            subject.addObserver(o);
        }
    }

    public void deleteObserver(Observer<E> o, String sub) {
        deleteObserver(o, Arrays.asList(sub));
    }

    public void deleteObserver(Observer<E> o, List<String> subs) {
        for (String sub : subs) {
            Subject<E> subject = subjects.remove(sub);
            if (subject != null) {
                subject.deleteObservers();
            }
        }
    }

    public void publish(String sub, E e) {
        Subject<E> subject = subjects.get(sub);
        if (subject != null) {
            subject.notifyObservers(e);
        }
    }
}
